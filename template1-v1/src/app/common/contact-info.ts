export class ContactInfo {

  constructor(
    public email: string = '',
    public phone: string = '',
    public website: string = '',
    public address: string = ''
  ) {
  }

}
