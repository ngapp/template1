import { MarkdownDetailInfo } from '../../../detail/markdown-detail-info';
import { MarkdownDetailEntry } from '../../../detail/markdown-detail-entry';


export var detailInfoNgAppHeader = new MarkdownDetailInfo(
  "ngapp-header",
  "Prolog",
  [
    new MarkdownDetailEntry(
      "Cover Letter",
      "",
      // "contents/details/ngapp-header.cover.md"
    ),

  ]
);
