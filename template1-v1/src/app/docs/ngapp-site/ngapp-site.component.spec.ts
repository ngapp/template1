import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgAppSiteComponent } from './ngapp-site.component';

describe('SetupDocComponent', () => {
  let component: NgAppSiteComponent;
  let fixture: ComponentFixture<NgAppSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgAppSiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgAppSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
