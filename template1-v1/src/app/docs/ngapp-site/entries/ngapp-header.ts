import { MarkdownDocEntry } from '../../../entry/markdown-doc-entry';

export var docEntryNgAppHeader = new MarkdownDocEntry(
  "ngapp-header",
  "Prolog",
  "Welcome to my site",
  "",
  // "contents/entries/ngapp-header.md"
);
