import { MarkdownDocEntry } from '../../../entry/markdown-doc-entry';

export var docEntryNgAppFooter = new MarkdownDocEntry(
  "ngapp-footer",
  "Epilog",
  "Parting statement",
  "",
  // "contents/entries/ngapp-footer.md"
);
