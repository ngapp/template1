import { ContactInfo } from '../../../common/contact-info';

export var myContactInfo = new ContactInfo(
  "info@ngapp.com",
  "555-555-5555",
  "http://www.ngapp.com/"
);
